---
title: "Web hosting"
publishDate: 2024-12-25
layout: permalink
description: "I currently host my website at NearlyFreeSpeech.NET."
---

In November 2020, I shared that I had left Cloudflare and GitLab Pages. Over four years later, I am still using [deSEC](https://desec.io/) for DNS hosting and [NearlyFreeSpeech.NET (NFSN)](https://www.nearlyfreespeech.net/) for shared web hosting. *If you're concerned or confused by the latter organization's name, I recommend reading [my November 2020 post](/cloudflare/) and [their January 2021 post.](https://blog.nearlyfreespeech.net/2021/01/19/free-speech-in-2021/)*

I also ["self-host"](https://en.wikipedia.org/wiki/Self-hosting_(web_services)) [several web applications and services](/resources/#software) on a [virtual private server (VPS)](https://en.wikipedia.org/wiki/Virtual_private_server) from Hetzner Cloud. I've been happy with NFSN's pricing and reliability, and I don't see a need to migrate this website to my VPS. I will update this page if that changes.

Please feel free to reach out with any questions!
