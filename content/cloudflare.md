---
title: "Personal post: I left Cloudflare"
date: 2020-11-02T19:00:00
tags: ['Miscellaneous']
categories: ['November 2020']
year: ['2020']
url: cloudflare
description: "I explain my decision to switch managed DNS providers and web hosting services."
---
***Note:** I now maintain [a separate page](/hosting/) to share hosting-related updates.*

I wanted to leave Cloudflare for a number of reasons (e.g., [its antagonistic behavior toward Tor users](https://blog.torproject.org/trouble-cloudflare) and [its role in maintaining a centralized Internet infrastructure](https://www.androidpolice.com/2020/07/17/cloudflare-accidentally-turns-off-half-the-internet-in-brief-but-major-outage/)). That isn't to say they don't also contribute to digital freedom by, for example, [challenging "national security letters."](https://www.eff.org/deeplinks/2018/02/twilio-demonstrates-why-courts-should-review-every-national-security-letter)

For managed DNS, I have switched to [deSEC](https://desec.io/), which is "organized as a non-profit charitable organization based in Berlin." deSEC is also based on open-source software and "free for everyone to use."

# What I had to consider next

In leaving Cloudflare, I lost support for HSTS, page rules, etc. I needed the ability to set *proper* [HTTP headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers), which isn't possible with GitLab Pages alone.

1. **DigitalOcean:** App Platform, which allows for three free static sites, only supports GitHub repositories. I don't like to use GitHub. For reasons why, see, for example, their [inadequate response to working with ICE](https://github.blog/2019-10-09-github-and-us-government-developers/).

2. **GitHub Pages:** Like GitLab Pages, GitHub Pages doesn't allow for custom HTTP headers. I also want to avoid supporting GitHub (see above) or its parent company, Microsoft.

3. **Netlify:** The free tier only integrates with Bitbucket, GitHub, and GitLab's SaaS instance. I prefer to use [Framagit](https://framagit.org/), which is based on GitLab but hosted by Framasoft, and Gitea (e.g., [Disroot's](https://git.disroot.org/) instance). I figured that if I'm manually deploying the website, I might as well select a different hosting service with more features.

4. I didn't want to support Amazon or Microsoft by using **AWS** or **Azure.**

# Why I chose NearlyFreeSpeech.NET

1. **I wanted to learn.**

> *One of the big areas where we differ from other providers is [member support](https://www.nearlyfreespeech.net/services/support). With the typical approach, the provider builds a (significant) charge for technical support into the basic fee for the service, and then uses leftovers from people who don't need support to subsidize the cost of providing support to those who need a lot. Our members, by and large, do not require typical member support, either because they are already very knowledgeable or are willing and able to learn on their own.*

2. [**It supports Let's Encrypt**](https://community.letsencrypt.org/t/web-hosting-who-support-lets-encrypt/6920), "a free, automated, and open [certificate authority](https://letsencrypt.org/about/) brought to you by the nonprofit Internet Security Research Group (ISRG)."

3. [**The "pay only for what you use" model**](https://www.nearlyfreespeech.net/services/pricing) means it shouldn't cost me more than [a few dollars per year](https://www.nearlyfreespeech.net/estimate).

4. The most compelling reason for me, though, was that **NearlyFreeSpeech.NET has consistent values.**

Example from their [frequently asked questions](https://www.nearlyfreespeech.net/about/faq#BecauseFuckNazisThatsWhy):

> *Because we believe in free speech, we host a small amount of offensive content. Some days, that's really hard to do. There are views expressed using our service that we find personally repugnant. [...] When we find a repugnant site on our service, we mark the account. We receive reports about all payments to such accounts, and we take a portion of that money larger than the amount of estimated profit and we donate it to the best organization we can find. The best organization in any given case meets two criteria:*

> 1. The recipient organization does share our values.
> 2. The recipient organization is as opposite (and hopefully as offensive) as possible to the site operator that funded the donation.

> *Examples of organizations that have received funding over the years include the Anti-Defamation League, the Southern Poverty Law Center, local chapters of the NAACP, the National Bail Fund Network, the American Immigration Council, the Trevor Project, and others.*

Example from their [privacy policy](https://www.nearlyfreespeech.net/about/privacy):

> *Cooperation with law enforcement authorities from other countries and cooperation when it is not legally required are at our sole discretion. Our discretion looks favorably on freedom and justice, and unfavorably on oppression and violence.*

# What this change means for you

I'm not leaving the [static website framework](https://en.wikipedia.org/wiki/Static_web_page), which means load times shouldn't change much (if at all). Cloudflare's caching didn't do much to improve load times.

Let's Encrypt certificates are [trusted by all major root programs](https://letsencrypt.org/2018/08/06/trusted-by-all-major-root-programs.html). You can test any website's security with [the Mozilla Observatory](https://observatory.mozilla.org/). There shouldn't be any lingering issues for this website, especially now that I can set custom headers.

I'm still hosting my personal wiki on GitLab Pages (the [Framagit](https://framagit.org/) instance) for the time being. Like this website, however, it is no longer behind Cloudflare.
