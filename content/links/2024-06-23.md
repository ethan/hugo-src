---
title: "Equity and social science"
date: 2024-06-23T10:45:00
tags: ['Data and research methods', 'Public policy']
categories: ['June 2024']
year: ['2024']
description: "These two articles discuss the importance of equity in the sciences."
---

[Fouché, R. (2024, Spring). Embracing the social in social science. *Issues in Science and Technology, 40*(3), 78--83.](https://issues.org/social-science-trust-fouche/)

> Most importantly, the formation of questions itself is a site of power. The questions we as a society ask science to address both reflect and create the values and power dynamics of social systems, whether the scientific disciplines recognize this influence or not. [...] Moving forward, how do we, as researchers, develop questions that not only welcome intellectual variety within the sciences but also embrace the diversity represented in societies?

[Plamondon, K. M., & Shahram, S. Z. (2024). Defining equity, its determinants, and the foundations of equity science. *Social Science and Medicine, 351*, 116940.](https://doi.org/10.1016/j.socscimed.2024.116940)

> This introduction to equity science is an invitation and challenge to others in global, public and population health and health sciences to move beyond an EDI rhetoric. We offer a structured set of methods, tools, and approaches to inform the transformational, interventional, and systems-changing work of collectively shaping more equitable futures at the heart of our shared aspirations for closing the gap in *this* generation.
