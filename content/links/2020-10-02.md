---
title: Facebook; Subminimum wage; Trump and COVID-19
date: 2020-10-02T19:00:00
tags: ['COVID-19', 'Free software', 'Health', 'Public policy']
categories: ['October 2020']
year: ['2020']
slug: Trump and health care
description: "Living wages for all. Medicare for All. Privacy is a human right."
---

[Press, A. (2020, October 2). It's time to end the subminimum wage for tipped workers. *Jacobin.*](https://jacobinmag.com/2020/10/tipped-restaurant-workers-waiters-coronavirus-subminimum-wage/)

> *Some progress has already been made toward ending the subminimum wage. So far, seven states have legislated an end to the practice. While there’s movement in other locales, it is plagued by carve-outs that exclude certain tipped workers (in New York, Governor Andrew Cuomo’s order excludes restaurant workers, the largest group of tipped workers in the state).*

[Sankin, A., & Mattu, S. (2020, October 1). *I scanned my favorite social media site on Blacklight and it came up pretty clean. What's going on?* The Markup.](https://themarkup.org/ask-the-markup/2020/10/01/i-scanned-my-favorite-social-media-site-on-blacklight-and-it-came-up-pretty-clean-whats-going-on)

> *But the main reason Facebook follows you around its site is to build detailed profiles of each user’s interests in order to target ads that presumably will be more successful than non-personalized ads, since they’re in some way connected to things the user sought out on his or her own.*

[Scott, D. (2020, October 2). *While Trump gets the best health care in the world, he wants to eliminate coverage for millions.* Vox.](https://www.vox.com/21498783/trump-positive-covid-19-coronavirus-test-obamacare-supreme-court)

> *After testing positive for the coronavirus, President Donald Trump can rest assured he will receive the best medical care without having to worry about the cost. [...] But the challenges some Americans have faced in the pandemic could only get worse under the Trump agenda. Millions could lose coverage outright and protections for preexisting conditions could be overturned if the Trump administration’s argument prevails at the high court.*
