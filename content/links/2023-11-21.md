---
title: "Gaza"
date: 2023-11-21T19:00:00
tags: ['Health', 'Mental health', 'Public policy']
categories: ['November 2023']
year: ['2023']
slug: Free Palestine
description: "These toolkits were created by librarians and other information professionals who support Palestinian freedom."
---

[Asian/Pacific American Librarians Association. (2023, October 30). *APALA statement calling for ceasefire in Gaza.*](https://www.apalaweb.org/gaza-ceasefire-2023)

> We call for an immediate ceasefire and access to critical resources and humanitarian aid restored. Join us in taking action: [Call to action toolkit](https://docs.google.com/document/u/0/d/1hpHkM9KlH5Yn3xq7nk9xfPtIkWZDblWnCKD8xt5DBx0/mobilebasic) [Google Docs].

[Librarians and Archivists with Palestine. (n.d.). *Our history.*](https://librarianswithpalestine.org/about/our-history/)
* [2023 statement on Gaza](https://librarianswithpalestine.org/2023-statement-on-gaza/)
* [Gaza 2023 action toolkit](https://docs.google.com/document/d/1vCZrTKiSitLonc8Csz0nMPqMgLLmLTrrPjT3s--obcU/edit) [Google Docs]

[We Here. (n.d.). *About us.*](https://www.wehere.space/about)
* [Resources for a free Palestine](https://docs.google.com/document/d/1_8Tpe9FQ1khUlAXJxdvZTOre8zdvbiqf7-qbR5-gS14/edit?usp=sharing) [Google Docs]
