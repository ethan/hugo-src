---
title: Protests and riots
date: 2021-01-08T10:00:00
tags: ['Black Lives Matter', 'Climate crisis', 'Education', 'Public policy']
categories: ['January 2021']
year: ['2021']
description: "Who would have have thought there are double standards in law enforcement?"
---

[Eligon, J. (2021, January 8). Racial double standard of Capitol Police draws outcry. *The New York Times.*](https://www.nytimes.com/2021/01/07/us/capitol-trump-mob-black-lives-matter.html)

> *Similar scenes unfolded all summer, as police officers clashed with scores of Black Lives Matter protesters. Many times, officers used batons and chemical agents to disperse crowds. And so what Ms. Elzie saw on television Wednesday afternoon infuriated her: A mob of mostly white Trump supporters stormed past police officers and vandalized the United States Capitol while officers, after initially offering resistance, mostly stood by. Some officers parted barricades, others held doors open and one was seen on video escorting a woman down steps.*

[Montanaro, D. (2021, January 7). *Timeline: How one of the darkest days in American history unfolded.* National Public Radio.](https://text.npr.org/954384999)

> *It was all egged on by a sitting president, who has been unable to accept losing his bid for reelection and who persuaded millions of his followers to buy into baseless, debunked and disproved conspiracy theories. The result: A mob violently storming and occupying the U.S. Capitol for hours, while staffers and lawmakers were evacuated or hid in fear.*

[Richie, D. (2021, January 7). *We can't gaslight students about the Capitol riot. We can use it as a teaching tool.* Chalkbeat.](https://www.chalkbeat.org/2021/1/7/22219245/classroom-lessons-from-capitol-hill-insurrection/)

> *Teachers: I wish the world would recognize how much pressure is on your shoulders. Young people are asking and thinking about these critical questions while teachers are also trying to teach math. It’s a lot, but the space you open for students can be transformative. Thank you.*

[Schwartz, J. (2021, January 7). Capitol rioters walked away. Climate protesters saw a double standard. *The New York Times.*](https://www.nytimes.com/2021/01/07/climate/capitol-riots-arrests-climate.html)

> *Even so, the apparently light treatment of Wednesday’s protesters troubled Kate Ruane, senior legislative counsel at the American Civil Liberties Union, who said that the treatment of the predominantly white crowd was far more gentle than the police response to many of the antiracism protesters and climate change activists of color, including the clearing last year of Lafayette Square in Washington with chemical irritants and force, and the treatment of Native American activists and others opposing pipeline projects.*
