---
title: Environments
date: 2020-08-20T19:00:00
tags: ['Climate crisis', 'Mental health', 'Public policy']
categories: ['August 2020']
year: ['2020']
description: "These two 'environments' are facing problems: The city environment (noise pollution) and the global environment (the climate crisis)."
---

[Flaxbart, M. (Producer). (2020, August 19). The city that never sleeps [Audio podcast episode]. In *Twenty Thousand Hertz.*](https://www.20k.org/episodes/citythatneversleeps)

> *When writer Paige Towers moved to one of the loudest cities in the world, she found herself overcome with anxiety and depression. She came to realize that the noise of the city itself, and the inability to escape from it, was making a huge impact on her mental health. [...] But the negative health implications of noise pollution are anything but simple.*

[Uhlmann, D. M. (2020, August 19). *The climate crisis is still a crisis.* The Atlantic.](https://www.theatlantic.com/ideas/archive/2020/08/climate-crisis-still-crisis/615319/)

> *If we fail to limit greenhouse-gas emissions by 2030, searing heat, widespread drought, destructive storms, and coastal flooding will become even more common. Rising oceans will envelop coastal cities such as Miami, New York, Boston, New Orleans, and Houston. The Pentagon predicts that mass migration and climate refugees will lead to widespread political instability.*
