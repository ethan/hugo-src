---
title: There's more to the 2020 elections
date: 2020-11-10T19:00:00
tags: ['Public policy']
categories: ['November 2020']
year: ['2020']
slug: post-election
description: "'If you hear people tell you to just shut up and celebrate and take some time off, they are ignoring the insomnia of money.'"
---

[Cassidy, J. (2020, November 10). Joe Biden must be a president for America's workers. *The New Yorker.*](https://www.newyorker.com/news/our-columnists/joe-biden-must-be-a-president-for-americas-workers)

> *There are other areas where the new Administration will have the capacity to move ahead without Republican approval. On its own accord, it can protect the interests of workers in future trade agreements; provide some student-loan relief for heavily indebted people on modest incomes; appoint financial regulators who are serious about rooting out wrongdoing and abusive behavior; and use antitrust policy to tackle the monopoly power that enables many large corporations to gouge their customers.*

[Kumar, A., Cooper, D., & Worker, J. (2020, November 10). Voters chose more than just the president: A review of important state ballot initiative outcomes. Working Economics Blog.](https://www.epi.org/blog/voters-chose-more-than-just-the-president-a-review-of-important-state-ballot-initiative-outcomes/)

> *Thus far they [Republicans] have not lost control of those previously controlled bodies (results in Arizona are still pending). Additionally, they have picked up both the House and the Senate in New Hampshire, giving Republicans total control in 23 states. Democrats control the state house and governor’s seat in 15 states, while 12 states have divided governance.*

[Sirota, D. (2020, November 8). Before the dust has settled, corporate Democrats are already attacking AOC and the Left. *Jacobin.*](https://jacobinmag.com/2020/11/alexandria-ocasio-cortez-democrats-aoc-biden-trump)

> *If you hear people tell you to just shut up and celebrate and take some time off, they are ignoring the insomnia of money. Corporate interests don’t rest -- they are like a T-1000 Terminator interminably pursuing their prime directives, which is to continue enriching the billionaire class. The election has not deterred them, which means we sleep at our own peril.*
