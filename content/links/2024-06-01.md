---
title: "Food stamps and cash assistance during 2023"
date: 2024-06-01T11:00:00
tags: ['Health', 'Poverty', 'Public policy'] 
categories: ['June 2024']
year: ['2024']
slug: SNAP and TANF in 2023
description: "SNAP and TANF are two important anti-poverty programs."
---

**Supplemental Nutrition Assistance Program (SNAP), or food stamps**
* [Food and Nutrition Service. (n.d.). *Supplemental Nutrition Assistance Program (SNAP).* U.S. Department of Agriculture.](https://www.fns.usda.gov/snap/supplemental-nutrition-assistance-program)
* [Food and Nutrition Service. (2023, December 28). *A short history of SNAP.*](https://www.fns.usda.gov/snap/short-history-snap)

[Urban Institute. (2024, May 20). *Does SNAP cover the cost of a meal in your county?*](https://www.urban.org/data-tools/does-snap-cover-cost-meal-your-county)

> *The year before, in 2022, the inflation adjustment to SNAP benefits improved the benefits' adequacy, and the share of counties with a gap between SNAP benefits and the cost of a meal dropped from 99 to 78 percent. However, this progress eroded almost entirely in 2023.*

**Temporary Assistance for Needy Families (TANF), or cash assistance**
* [Administration for Children and Families. (2022, June 28). *About TANF.*](https://www.acf.hhs.gov/ofa/programs/tanf/about)

[Azevedo-McCaffrey, D., & Aguas, T. (2024, May 29). *Continued increases in TANF benefit levels are critical to helping families meet their needs and thrive.* Center on Budget and Policy Priorities.](https://www.cbpp.org/research/income-security/continued-increases-in-tanf-benefit-levels-are-critical-to-helping)

> *Only 11 states have benefits that have kept up with inflation since 1996, and several of those are only due to recent increases and very low benefit levels. States can do more to make sure families can access TANF and have the assistance they need to meet basic needs and thrive.*

[Falk, G. (2024, April 3). *The Temporary Assistance for Needy Families (TANF) block grant.* Congressional Research Service](https://crsreports.congress.gov/product/pdf/IF/IF10036)

> *The TANF block grant has not been increased since the enactment of the 1996 welfare law. There has been no adjustment for inflation or population change. From 1997 to 2023, the basic TANF block grant has lost 47% of its value to inflation. During TANF's history, states have at times received TANF funds in addition to the basic block grant. Since 2011, some states have routinely tapped a "contingency fund" that was originally intended to provide extra funding during economic recessions.*
