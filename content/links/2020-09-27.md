---
title: Amy Coney Barrett
date: 2020-09-27T19:00:00
tags: ['Disability', 'Public policy']
categories: ['September 2020']
year: ['2020']
description: "Amy Coney Barrett was nominated by Donald Trump to replace Ruth Bader Ginsburg."
---

[Sins Invalid. (2020, September 26). *Amy Coney Barrett is a threat to disability justice.*](https://www.sinsinvalid.org/news-1/2020/9/25/amy-coney-barrett-is-a-threat-to-disability-justice)

> *Judge Barrett’s opposition to the Affordable Care Act, abortion rights, and LGBTQ rights is an affront to these values and the rights and dignity of all people. At a time when immigrants are being subjected to forced sterilizations, trans people are being subjected to violence and attacks on their legal freedoms, and Black and disabled people’s lives are being treated as disposable by both police and our country’s response to COVID-19, we need a Supreme Court that will stand against these injustices.*


[Toobin, J. (2020, September 26). There should be no doubt why Trump nominated Amy Coney Barrett. *The New Yorker.*](https://www.newyorker.com/news/daily-comment/there-should-be-no-doubt-why-trump-will-nominate-amy-coney-barrett)

> *The judge has described herself as a “textualist” and an “originalist” -- the same words of legal jargon that were associated with Scalia. [...] But these words are abstractions. In the real world, they operate as an agenda to crush labor unions, curtail environmental regulation, constrain the voting rights of minorities, limit government support for health care, and free the wealthy to buy political influence.*
