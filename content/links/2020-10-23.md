---
title: Schools, search engines, and access to information
date: 2020-10-23T19:00:00
tags: ['COVID-19', 'Education', 'Free culture', 'Miscellaneous']
categories: ['October 2020']
year: ['2020']
slug: access to information
description: "Please promote the use of open educational resources."
---

[Featherstone, L. (2020, October 18). Public schools are starting to charge “fees” for “childcare.” *Jacobin.*](https://jacobinmag.com/2020/10/public-schools-charging-childcare-coronavirus/)

> *Durham, North Carolina announced in August that it would be charging $70 to $140 per week (homeless and foster families will not be charged, no doubt cold comfort to the majority) for its “learning centers,” coincidentally located in school buildings and providing “supervision” to kids during the day. (Otherwise known as "schools” providing “school.”) [...] Of course, teachers and students should not go back until it’s safe, but all defenders of public education need to be on notice that when districts insult the public by charging money for public schooling, they’re building a constituency for the Right.*

[Mir, R. (2020, October 22). *Open education and artificial scarcity in hard times.* Electronic Frontier Foundation.](https://www.eff.org/deeplinks/2020/10/open-education-and-artificial-scarcity-hard-times)

> *Despite the many benefits of open access and open education, most instructors have still never heard of OER [open educational resources]. This means a simple first step away from an expensive and locked down system of education is to make sure you make the benefits of OER more widely known. While pushing for the broader utilization of OER, we must advocate for systemic changes to make sure OER is supported on every campus.*

[Ochigame, R. (2020, August 31). Informatics of the oppressed. *Logic Magazine.*](https://logicmag.io/care/informatics-of-the-oppressed/)

> *If yesterday’s information scientists claimed that their models ranked authors by "productivity" and libraries by "effectiveness," today’s "AI experts" claim that their algorithms rank "personalized" search results by "relevance." These claims are never innocent descriptions of how things simply are. Rather, these are interpretive, normative, politically consequential prescriptions of what information should be considered relevant or irrelevant. These prescriptions, disguised as descriptions, serve to reproduce an unjust status quo.*
