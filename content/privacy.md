---
title: Privacy policy
publishDate: 2020-11-08
lastmod: 2024-11-12
layout: permalink
description: "You should always try to understand the privacy policy."
---

This website uses [a self-hosted version](https://github.com/umami-software/umami#readme) of [Umami](https://umami.is/) for analytics; I run Umami on a [Hetzner Cloud](https://www.hetzner.com/cloud/) server located in Virginia.

There are no other external dependencies or resources, including Google Fonts or social media (e.g., Facebook, Twitter, and embedded YouTube videos). If JavaScript is disabled (typically a conscious decision) or the search script fails to load, however, search queries will instead be sent to DuckDuckGo.

I do not have access logs enabled. [Access logs](https://httpd.apache.org/docs/current/logs.html) typically include IP addresses, operating system information, the internal pages and resources requested, and time of access.

Browsers will, by default, "prefetch," or ["proactively perform domain name resolution on both links that the user may choose to follow as well as URLs for items referenced by the document."](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-DNS-Prefetch-Control) Because I link to external sites, I have *disabled* prefetching with the X-DNS-Prefetch-Control header. Setting this header avoids "leaking" your information to external sites by simply browsing my website. **If you click on an external link, you are still subject to the privacy and security policies of that outside website.**

Your connection to this website is secured by, among other settings, [HTTP Strict Transport Security (HSTS)](https://hstspreload.org/) headers, redirection to HTTPS, and a strong Content Security Policy. You can [read more about web security](https://infosec.mozilla.org/guidelines/web_security) or [easily test any website's security settings.](https://developer.mozilla.org/en-US/observatory/analyze?host=ethanyoo.com)

Your email address is considered personal information under, for example, the [California Consumer Privacy Act of 2018.](https://www.oag.ca.gov/privacy/ccpa) That being said, I have no interest in sharing *anyone's* information with third parties.

If you do contact me, you're not required to use a real name or a permanent email address. `1D2E ED2F 9D90 6875 C4F4 DAD2 068C 9178 672B A80E` is the fingerprint for my OpenPGP key, which you can find in the footer. *Don't feel obligated to use OpenPGP when contacting me!* If an encrypted response is requested, I can also [set up a temporary inbox.](https://kb.mailbox.org/en/private/e-mail-article/temporary-mailbox-for-external-users) If you're wondering why encryption is so important, consider reading ["Why we encrypt" by Bruce Schneier.](https://www.schneier.com/blog/archives/2015/06/why_we_encrypt.html)

*First posted on **November 8, 2020***\
*Updated on **December 11, 2020** after disabling access logs*\
*Updated on **December 29, 2022** after adding the website's search function*\
*Updated on **March 12, 2023** to move a sentence describing the website's search function*\
*Updated on **June 2, 2024** to reflect the use of Plausible Community Edition and to change some wording*\
*Updated on **July 8, 2024** to replace the link to Mozilla Observatory ([deprecated](https://developer.mozilla.org/en-US/blog/mdn-http-observatory-launch/)) with a link to the [MDN HTTP Observatory](https://developer.mozilla.org/en-US/observatory)*\
*Updated on **November 12, 2024** to replace Plausible Community Edition with Umami*
