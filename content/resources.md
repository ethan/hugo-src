---
title: Resources
publishDate: 2021-08-19
layout: permalink
description: "Browse the resources I use and recommend, including articles and free software."
toc: true
---

## Educational resources and open licenses {#OER}

[Creative Commons](https://creativecommons.org/)

[Free Software Foundation: Free software resources](https://www.fsf.org/resources/)

[Open Educational Resources (OER Commons): Search resources](https://www.oercommons.org/browse)

[Saylor Academy: College Success](https://learn.saylor.org/course/view.php?id=700)

## Recommended reading {#reading}

[About Feeds: What is a feed?](https://aboutfeeds.com/)

[Annamma, S. A. (2018). *The pedagogy of pathologization: Disabled girls of color in the school-prison nexus.* Routledge.](https://doi.org/10.4324/9781315523057)

[DeVault, D. (2024, September 25). Neurodivergence and accountability in free software. *Drew DeVault's blog.*](https://drewdevault.com/2024/09/25/2024-09-25-Neurodivergence-and-accountability-in-free-software.html)

Lydia X. Z. Brown on *Autistic Hoya*

* [Ableism is not "bad words." It's violence.](https://www.autistichoya.com/2016/07/ableism-is-not-bad-words-its-violence.html)
* [Ableism/language](https://www.autistichoya.com/p/ableist-words-and-terms-to-avoid.html)
* [Violence in language: Circling back to linguistic ableism.](https://www.autistichoya.com/2014/02/violence-linguistic-ableism.html)

[Pronouns.org (formerly MyPronouns.org): Resources](https://pronouns.org/resources)

[Opsahl, K. (2013, June 7). *Why metadata matters.* Electronic Frontier Foundation.](https://www.eff.org/deeplinks/2013/06/why-metadata-matters)

[Rogers, C. (2016). *Intellectual disability and being human: A care ethics model.* Routledge.](https://doi.org/10.4324/9781315638713)

[Stevenson, B. (2014). *Just mercy: A story of justice and redemption.* Spiegel & Grau.](https://bookshop.org/p/books/just-mercy-a-story-of-justice-and-redemption-bryan-stevenson/9406702)\
[Stevenson, B. (2014). *Just mercy: A story of justice and redemption.* [Audiobook]. Penguin Random House Audio.](https://libro.fm/audiobooks/9780553550610)

## Recommended software {#software}

I rely on [free software](https://en.wikipedia.org/wiki/Free_software) every day. Described below are the services and tools I use most (i.e., at least once a week and often daily). Please feel free to contact me for other alternatives or public instances of self-hostable services!

{{<icon "/img/icon/authelia.svg">}} [Authelia](https://www.authelia.com/): Self-hosted single sign-on (SSO) service\
Licensing: Apache License 2.0\
*Could replace the built-in authentication on individual self-hosted services and add authentication to services that do not have built-in authentication*

{{<icon "/img/icon/bitwarden.svg">}} [Bitwarden](https://bitwarden.com/): Self-hostable password manager\
Licensing: GNU Affero General Public License v3.0\
*Could replace 1Password, Dashlane, and LastPass*

{{<icon "/img/icon/cryptomator.svg">}} [Cryptomator](https://cryptomator.org/): Client-side file encryption solution that works well with cloud storage\
Licensing: GNU General Public License v3.0\
*Could replace Boxcryptor*

{{<icon "/img/icon/firefly-iii.svg">}} [Firefly III](https://firefly-iii.org/): Self-hosted personal finance manager\
Licensing: GNU Affero General Public License v3.0\
*Could replace Mint and YNAB, albeit with a ["fundamentally different"](https://docs.firefly-iii.org/explanation/more-information/what-its-not/) [philosophy](https://docs.firefly-iii.org/explanation/firefly-iii/background/personal-finances/)*

{{<icon "/img/icon/freshrss.svg">}} [FreshRSS](https://freshrss.org/): Self-hosted news aggregator\
Licensing: GNU Affero General Public License v3.0\
*Could replace Feedly and Inoreader*

{{<icon "/img/icon/gnupg.svg">}} [GnuPG](https://gnupg.org/): Used by Keyoxide's [decentralized identity verification](https://docs.keyoxide.org/) and mailbox.org's [encrypted inbox](https://mailbox.org/en/security), "GnuPG is a complete and free implementation of the OpenPGP standard as defined by RFC4880" \
Licensing: GNU General Public License v2.0 or later\
*See also [Git's "Signing Your Work"](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) and [Keyoxide](https://keyoxide.org/hello@ethanyoo.com)*

{{<icon "/img/icon/grist.svg">}} [Grist](https://www.getgrist.com/): A "modern relational spreadsheet" that supports Excel and Python functions\
Licensing: Apache License 2.0 for non-enterprise features\
*Could replace Airtable and Google Sheets*

{{<icon "/img/icon/linkding.svg">}} [linkding](https://github.com/sissbruecker/linkding#readme): Simple bookmark tool that can automatically save bookmarked pages to the [Internet Archive's Wayback Machine](https://archive.org/web/)\
Licensing: MIT (Expat) License\
*See also [ArchiveBox](https://archivebox.io/)*

{{<icon "/img/icon/mailboxdotorg.svg">}} [mailbox.org](https://mailbox.org/en/): Secure mail service provider\
Licensing: Based on [Open-Xchange, Postfix, and other free software](https://userforum-en.mailbox.org/topic/is-everything-at-mailbox-org-open-source)\
*Could replace Gmail and Outlook*

{{<icon "/img/icon/mozillafirefoxbrowser.svg">}} [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/) and {{<icon "/img/icon/mozillathunderbird.svg">}} [Mozilla Thunderbird](https://www.thunderbird.net/en-US/): Web browser (desktop and mobile) and email client that can also manage contacts, calendars, and feeds (desktop)\
Licensing: Mozilla Public License 2.0\
*Firefox could replace Microsoft Edge, Google Chrome, etc. while Thunderbird could replace eM Client, Mailbird, and Microsoft Outlook. See [FairEmail](https://email.faircode.eu/) for a mobile email client*

{{<icon "/img/icon/nextcloud-blue.svg">}} [Nextcloud](https://nextcloud.com/athome/): Self-hosted cloud storage and content collaboration platform\
Licensing: GNU Affero General Public License v3.0 or later (server)\
*Could replace Google Drive and Microsoft 365 -- CalDAV and CardDAV support also allow for synchronization of calendars and contacts, respectively*

{{<icon "/img/icon/ntfy.svg">}} [ntfy](https://ntfy.sh/): Self-hostable HTTP-based push notification service\
Licensing: Apache License 2.0 and GNU General Public License v2.0\
*See also [Gotify](https://gotify.net/)*

{{<icon "/img/icon/rclone.svg">}} [Rclone](https://rclone.org/): Cloud storage management tool (e.g., move files between different servers)\
Licensing: MIT (Expat) License\
*Could replace [a supported provider's](https://rclone.org/#providers) synchronization software (e.g., Google Drive or Microsoft OneDrive)*

{{<icon "/img/icon/restic.png">}} [restic](https://restic.net/): Cross-platform backup tool with data integrity verification, encryption, deduplication, and support for [multiple backends](https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html)\
Licensing: 2-Clause BSD License (Simplified BSD License) \
*See also [Awesome restic](https://github.com/rubiojr/awesome-restic#readme) for other restic-related projects*

{{<icon "/img/icon/shiori.svg">}} [Shiori](https://github.com/go-shiori/shiori#readme): Self-hosted read-it-later application\
Licensing: MIT (Expat) License\
*Could replace Instapaper and Pocket. See also [wallabag](https://wallabag.org/), which I used until early December 2022*

{{<icon "/img/icon/signal.svg">}} [Signal](https://signal.org/): Secure instant messaging\
Licensing: GNU Affero General Public License v3.0 (server, desktop) and GNU General Public License v3.0 (mobile)\
*Could replace text messages (SMS/MMS), Facebook Messenger, Telegram, and WhatsApp*

{{<icon "/img/icon/silverbullet.png">}} [SilverBullet](https://silverbullet.md/): Self-hosted note-taking application with [end-user programming features](https://silverbullet.md/End-User%20Programming)\
Licensing: MIT (Expat) License\
*See also [Joplin](https://joplinapp.org/) and [Logseq](https://logseq.com/), which are note-taking applications I've previously used.*

{{<icon "/img/icon/syncthing.svg">}} [Syncthing](https://syncthing.net/): Continuous file synchronization program\
Licensing: Mozilla Public License 2.0\
*See also [Syncthing-Fork](https://github.com/Catfriend1/syncthing-android#readme) (Android) and [Syncthing Tray](https://github.com/Martchus/syncthingtray#readme) (cross-platform)*

{{<icon "/img/icon/texstudio.svg">}} [TeXstudio](https://www.texstudio.org/): LaTeX editor\
Licensing: GNU General Public License v3.0\
*See also [The LaTeX Project](https://www.latex-project.org/) and [a TeX - LaTeX Stack Exchange answer comparing the TeX distributions](https://tex.stackexchange.com/a/239204)*

{{<icon "/img/icon/vikunja.svg">}} [Vikunja](https://vikunja.io/): Self-hostable task management application\
Licensing: GNU Affero General Public License v3.0\
*Could replace Microsoft To Do, Todoist, etc.*

[Zooming Out](https://adikos.net/blog/articles/zooming-out/): "A provisional list of Zoom replacements" that I helped research

## Web archives {#archives}

[End of Term (EOT) Web Archive](https://eotarchive.org/): "a collaborative initiative that collects, preserves, and makes accessible United States Government websites at the end of presidential administrations"
* [Searchable collections](https://web.archive.org/collection-search) are hosted by the [Internet Archive](https://archive.org/donate).

[Congressional websites at the end of each Congress since 2006](https://webharvest.gov/): Captured by the Internet Archive under contract with the National Archives and Records Administration (NARA)

[White House websites at the end of each presidential administration](https://www.archives.gov/presidential-records/research/archived-white-house-websites): Captured by NARA

[GovInfo](https://www.govinfo.gov/app/browse/author): "free public access to official publications from all three branches of the federal government"

[Time Travel](https://timetravel.mementoweb.org/): Search web archives that are publicly accessible via [the Memento protocol](https://mementoweb.org/depot/)

Other [web archiving inititatives](/2020/09/archives/) and [digital resources](https://www.archives.gov/nhprc/projects/digital-resources)

## For other content {#other}

[Read why, as of November 2, 2020,](/cloudflare/) I host this website on [NearlyFreeSpeech.NET.](https://www.nearlyfreespeech.net/)

[Browse the Git repositories](https://git.eyoo.link/ethan) for this website and its theme.

<a class="link-000" href=https://www.eff.org/><img src="/img/eff.png" alt="Join EFF!" title="Electronic Frontier Foundation (EFF)" /></a>
