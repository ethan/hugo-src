---
title: Now
publishDate: 2020-11-11
layout: permalink
description: "Find out what I am doing now: What I'm learning or reading, what I'm working on in my free time, and where I'm at professionally."
---

**You might also want to read:** [What is a "now page"?](https://nownownow.com/about)

## Recent personal or professional progress
* My first single-authored article, a brief report entitled "Exploring the increased prevalence of autism in the fee-for-service Medicare population with open data, 2007 to 2018", was accepted for publication in [*Disability and Health Journal*](https://www.sciencedirect.com/journal/disability-and-health-journal). This paper expands on [the project](https://github.com/ethyoo/NJLEND) I completed as a first-year NJLEND Fellow. It is now [available online](https://doi.org/10.1016/j.dhjo.2025.101776).

## What else I'm learning about or reading

* [Our World in Data's articles on artificial intelligence (AI)](https://ourworldindata.org/artificial-intelligence)

# In previous months
## January 2025

* I submitted a sixth and final PhD application for fall 2025 matriculation.
* I am again conducting research with a professor in the Department of Library and Information Science at Rutgers University School of Communication and Information.
* I [added](https://www.ntppool.org/a/ethanyoo) my [Network Time Protocol (NTP)](https://en.wikipedia.org/wiki/Network_Time_Protocol) server, which uses [`chrony`](https://chrony-project.org/), to the [NTP Pool Project](https://www.ntppool.org/).

## December 2024

* I visited Washington, DC in early December.
* I led [a presentation](https://peerta.acf.hhs.gov/content/tanf-disaster-response-preparedness-webinar) on the [Temporary Assistance for Needy Families (TANF) Disaster Preparedness and Response Playbook](https://www.acf.hhs.gov/ohsepr/policy-guidance/dcl-tanf-disaster-prep-response-playbook), which I co-authored.
* I spend time with friends, mainly from middle and high school, on New Year's Eve.
* I submitted five PhD applications.

## November 2024

* I attended [the AUCD Conference](https://www.aucd.org/conference) virtually.
* I set up [Authelia](https://www.authelia.com/), which can add single sign-on (SSO) authentication to self-hosted services.

## October 2024

* The [inaugural cohort](https://stateofthestates.ku.edu/cohort-1-ambassadors) for the [State of the States Data Ambassador Program](https://boggscenter.rwjms.rutgers.edu/news/njlend-fellow-ethan-yoo-selected-as-state-of-the-states-data-ambassador) had its last training session; we will continue to support the [State of the States in Intellectual and Developmental Disabilities](https://stateofthestates.ku.edu/) project and the [Association of University Centers on Disabilities (AUCD) network](https://www.aucd.org/network-members).

## September 2024

* I started working as an instructional assistant for a fall 2024 undergraduate course, [Retrieving and Evaluating Electronic Information](https://comminfo.rutgers.edu/courses/retrieving-and-evaluating-electronic-information), in Rutgers University's [Information Technology and Informatics (ITI)](https://comminfo.rutgers.edu/undergraduate-programs/information-technology-and-informatics-major) undergraduate program.

## August 2024

* I was [converted](https://www.ecfr.gov/current/title-5/chapter-I/subchapter-B/part-362/subpart-A/section-362.107) to a position in the competitive service.
* I saw friends from Swarthmore and went with one friend to [The Newark Museum of Art](https://newarkmuseumart.org/).
* I visited Washington, DC in mid-August to support [the inaugural convening on disaster human services](https://www.acf.hhs.gov/ohsepr/event/shaping-future-disaster-human-svcs).

## July 2024

* For the first time since the emergence of COVID-19, I went with several friends to [Blobfest](https://thecolonialtheatre.com/blobfest/), an annual event commemorating the 1958 film [*The Blob*](https://letterboxd.com/film/the-blob/).
* I attended an [NJLEND](https://boggscenter.rwjms.rutgers.edu/student-experiences/njlend) reunion at The Boggs Center on Disability and Human Development (formerly The Boggs Center on Developmental Disabilities).

## June 2024

* I wrapped up my second year as a fellow with [The Boggs Center](https://boggscenter.rwjms.rutgers.edu/). I will continue representing New Jersey in the [State of the States Data Ambassador Program](https://stateofthestates.ku.edu/data-ambassadors).

## May 2024

* I graduated from Rutgers University School of Communication and Information with a [Master of Information (MI) degree](https://comminfo.rutgers.edu/graduate-programs/master-information) and having completed the [data science concentration](https://comminfo.rutgers.edu/graduate-programs/master-information/concentrations/data-science).
* I graduated from the [Eagleton Graduate Fellowship Program in Politics and Government](https://eagleton.rutgers.edu/grad-fellowships/).

## April 2024

* My appointment as a research assistant with the Department of Library and Information Science ended in mid-April.
* My placement with the [New Jersey Early Intervention System (NJEIS)](https://www.nj.gov/health/fhs/eis/) in the Division of Family Health Services ended on April 30.

## March 2024

* I attended the [RU Running? Political Campaign Training](https://cypp.rutgers.edu/young-leaders/) held by the Center for Youth Political Participation at the Eagleton Institute of Politics.
* I visited the [Office of Human Services Emergency Preparedness and Response](https://www.acf.hhs.gov/ohsepr) (OHSEPR) in Washington, DC during my spring break.

## February 2024

* I went to Annapolis, MD with the Eagleton Graduate Fellows.

## January 2024

* I took the GRE General Test.
* I started an internship with [New Jersey Early Intervention System (NJEIS)](https://www.nj.gov/health/fhs/eis/) in the Division of Family Health Services, New Jersey Department of Health as part of the [Eagleton Graduate Fellowship Program](https://eagleton.rutgers.edu/grad-fellowships/).
* I started my last semester in the Master of Information (MI) program.

## December 2023

* I finished my second-to-last semester in the Master of Information (MI) program.

## November 2023

* I was selected to represent The Boggs Center on Developmental Disabilities and New Jersey as part of an inaugural cohort of "Data Ambassadors" led by the federally funded [State of the States in Intellectual and Developmental Disabilities](https://stateofthestates.ku.edu/) project and the [Association of University Centers on Disabilities (AUCD)](https://www.aucd.org/).
* I went to the [Museum of Sex](https://www.museumofsex.com/about/) with a friend.

## October 2023

* I attended the [Princeton Prospective PhD Preview (P3)](https://graddiversity.princeton.edu/prospective-phd-preview-p3) program, "a nationally recognized program that focuses on engaging scholars from diverse academic, research, and non-traditional backgrounds around the path to the Ph.D."
* I returned to work with a professor from the Department of Library and Information Science at Rutgers University School of Communication and Information.
* I attended the Eagleton Institute's [Future of Democracy Networking Reception](https://eagleton.rutgers.edu/event/future-of-democracy-networking-reception/).
* I went to the [Museum of Modern Art (MOMA)](https://www.moma.org/) with a friend.

## September 2023

* I started the fall 2023 semester at Rutgers University, including the [Eagleton Graduate Fellowship Program in Politics and Government](https://eagleton.rutgers.edu/grad-fellowships/).
* I wrapped up my role with the Center for Research on Ending Violence (REV) at Rutgers School of Social Work.

## August 2023

* I returned to [The Boggs Center's NJLEND program](https://boggscenter.rwjms.rutgers.edu/student-experiences/njlend) as a Second-Year Fellow.
* I visited Washington, DC in early August.
* I was invited to continue working at the Administration for Children and Families (ACF) as a part-time intern. I will remain in the [Office of Human Services Emergency Preparedness and Response](https://www.acf.hhs.gov/ohsepr) (OHSEPR).

## July 2023

* I started a new project at the Center for Research on Ending Violence (REV) related to sexual misconduct by physicians.

## June 2023

* I attended the annual New Jersey Library Association (NJLA) Conference.
* I completed an [8-week introduction to grant proposal writing](https://www.fhsu.edu/sociology/grant-writing-certification-program/) offered by the Sociology Program at Fort Hays State University.
* I started a full-time summer internship at the Administration for Children and Families (ACF), U.S. Department of Health and Human Services (HHS), where I am working with the [Office of Human Services Emergency Preparedness and Response](https://www.acf.hhs.gov/ohsepr) (OHSEPR).
* I was invited to continue working with the Center for Research on Ending Violence (REV) at Rutgers School of Social Work as a temporary research assistant on a new project.

## May 2023

* I graduated from the 2022-2023 [New Jersey Leadership Education in Neurodevelopmental and Related Disabilities (NJLEND) graduate fellowship program](https://boggscenter.rwjms.rutgers.edu/student-experiences/njlend).
* I accepted a Governor's Executive Award for the 2023-2024 [Eagleton Graduate Fellowship Program in Politics and Government](https://eagleton.rutgers.edu/grad-fellowships/).
* My initial appointment as a research assistant with the Department of Library and Information Science ended; I will return in August 2023.

## April 2023

* I completed the non-credit [Certificate in Nonprofit Career Development (Center for Nonprofits and Philanthropy, Texas A&M University)](https://tamu.badgr.com/public/assertions/ifg0paHpTSOBvVutj4_JsQ?identity__email=ethan@ethanyoo.com), for which I received a full scholarship.
* I was awarded an [Equity Scholarship](https://www.njla.org/scholarships) by the New Jersey Library Association (NJLA) for the 2023-2024 academic year. The Equity Scholarship was funded through partnerships with Rutgers University School of Communication and Information, the New Jersey State Library, and LibraryLinkNJ.
* I started working for the Center for Research on Ending Violence (REV) at Rutgers School of Social Work as a temporary graduate research assistant.

## March 2023

* I decided that I will pursue a research doctorate (i.e., a PhD) after graduating with the Master of Information (MI) degree - most likely in a social science (e.g., sociology or demography).
* I started working on a pilot study with a professor from the Department of Library and Information Science at Rutgers University School of Communication and Information.

## November 2022 to February 2023

* I interviewed for full-time employment with many interesting government and non-profit organizations, but I ultimately decided to continue attending school full-time while working part-time. For example, I started working for Rutgers University Libraries as a Data Science Graduate Specialist, a role in which I develop and teach weekly, open workshops in "Data Science Basics" for individuals who want to improve their data management and visualization skills.
* I created a [Letterboxd account](https://letterboxd.com/ethanyoo/stats/) to log the movies that I watch.
* I spent some time reviewing Esri tools (e.g., ArcGIS Pro) and self-hosting concepts (e.g., Docker containers and Ansible).

## October 2022

* I finished my year of national service in the [AmeriCorps VISTA](https://americorps.gov/serve/fit-finder/americorps-vista) program on October 24, 2022.

## September 2022

* I started the Master of Information (MI) degree program at the Rutgers University School of Communication and Information on a part-time basis.
* I started NJLEND, a graduate fellowship for which I was selected as the first Library and Information Science Fellow in May 2022.

## August 2022

* I took an American Heart Association (AHA) course to renew my Basic Life Support (CPR and AED) certification.
* I finished Optimization - Linear Programming at Thomas Edison State University.

## July 2022

* Using the scholarship I was awarded by Thomas Edison State University, I started a graduate-level course (Optimization - Linear Programming) in the data science program.
* I met with a group of friends from Swarthmore College in New York City.

## June 2022

* I was certified to graduate from Thomas Edison State University with a Bachelor of Arts in Computer Science and an associate's degree in mathematics on June 10, 2022. I also completed the undergraduate certificates in Computer Information Systems and Operations Management.
* I was awarded scholarship funds for the fall 2022 semester by the School of Communication and Information at Rutgers University.
* I was awarded a Joseph Lippincott Fellowship ([Swarthmore Fellowship](https://www.swarthmore.edu/fellowships-and-prizes/swarthmore-fellowships)) by the Fellowships and Prizes Committee at Swarthmore College.

## May 2022

* I finished the February 2022 term at Thomas Edison State University, during which I took Computer Architecture and the Liberal Arts Capstone.
* I was selected to participate in [The Boggs Center's Leadership Education in Neurodevelopmental and Related Disabilities (NJLEND) program](https://www.rwjms.rutgers.edu/boggscenter/student/LEND.html) as a Library and Information Science Fellow during the 2022-2023 academic year. NJLEND is an interdisciplinary graduate fellowship and leadership development program.

## February 2022

* I was [interviewed by someone from Saylor Academy](https://www.saylor.org/2022/02/student-spotlight-ethan-yoo/).
* I was awarded $2500 in scholarship funds by the Thomas Edison State University Scholarship Committee.

## January 2022

* I officially withdrew from the Master of Social Work (MSW) program at Smith College School for Social Work, from which I had been taking a leave of absence.
* I finished the November 2021 term at Thomas Edison State University, including three [online courses](https://www.tesu.edu/academics/online-courses) (Data Structures; Database Management; Software Engineering) and two [e-Pack courses](https://www.tesu.edu/academics/e-pack-courses) (Computer Concepts and Applications; Industrial Psychology).

## November 2021

* Between moving in January 2021 and summer coursework at Smith College, I had some months where I wasn't regularly volunteering as an EMT. I'm back to it as of late November 2021.

## December 2020

* I started participating in [#100DaysToOffload](https://100daystooffload.com/) on December 1. #100DaysToOffload "challenge[s] people to publish 100 posts on their **personal blog** in a year." I'm posting these entries to [Write.as,](https://ethan.writeas.com/) a blogging platform with its foundations in [free software](https://writefreely.org/) and [a respect for privacy.](https://write.as/principles)
* I disabled access logs and updated the [privacy policy](/privacy/) on December 24.
* I left Mastodon for a few weeks before creating a new account as [@ethanyoo@todon.eu](https://todon.eu/@ethanyoo) on December 29.

## November 2020

* I wrote my first "personal" post on November 2, where I describe [leaving Cloudflare and GitLab Pages.](/cloudflare/)
* I'm back to volunteering as an Emergency Medical Technician (EMT) as of November 4.
* I posted the [privacy and security policy](/privacy/) for this website on November 8.
* I wrote a new ["Start" page](/) on November 11; it's now more of a landing page. There is an image of the old index page at the bottom of this post.
* I added the "Now" page (what you're currently reading) on November 11.

# Images

![The index.html (a website's default starting page) until November 11, 2020. It included summaries of the four most recent posts, as well as the header and footer. The current index page has similar links, including a modified footer.](/img/index-old.png#center "The website's index (home) page until November 11, 2020.")
