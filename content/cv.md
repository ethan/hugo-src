---
title: Public CV
publishDate: 2024-12-26
layout: permalink
description: "View or download a public version of my CV."
---

Thanks to [a Hugo shortcode](https://github.com/anvithks/hugo-embed-pdf-shortcode) -- which I updated to use a more recent version of [PDF.js](https://mozilla.github.io/pdf.js/) -- it is possible to navigate this document using a set of "Previous" and "Next" buttons.

To use bookmarks and other links, please [download the PDF file](/Yoo-Ethan_CV-public.pdf). I am also happy to provide [a detailed CV by email.](mailto:hello@ethanyoo.com?subject=Website%20inquiry%20-%20Detailed%20CV)

{{< embed-pdf url="/Yoo-Ethan_CV-public.pdf" >}}
