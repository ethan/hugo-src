---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
tags: ['Black Lives Matter', 'Climate crisis', 'Disability', 'Education', 'Electronic Frontier Foundation (EFF)', 'Free culture', 'Free software', 'Gender', 'Health', 'Mental health', 'Miscellaneous', 'Public policy'] 
categories: ['August 2021']
year: ['2021']
slug: Hugo will hyphenate it
description: "I add a question, quote, or summary."
---

[Last, F. (Year, Month Date). Title. *The New York Times.*](https://www.nytimes.com/)

> *Block quote*

[Last, F. (Year, Month Date). *Title.* National Public Radio.](https://text.npr.org/)

> *Block quote*

Consider adding related links, pictures, or posts!
