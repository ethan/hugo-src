# Ethan Yoo

This repository contains the files used by a static site generator, [Hugo](https://gohugo.io/), to create [my personal website](https://ethanyoo.com/). Although I do sometimes reference my professional activities, the website's contents do not necessarily reflect the views of any other person or organization (e.g., government agencies or [the Friends of the Somerset County Library](https://fsclnj.org/)).

I use a [Git post-receive hook](https://git-scm.com/docs/githooks) to download Hugo and generate the static content on my web hosting service, which is currently [NearlyFreeSpeech.NET](https://www.nearlyfreespeech.net/). I explained in late 2020 [why I chose this hosting service](https://ethanyoo.com/cloudflare/); I also recommend reading their post on ["Free speech in 2021"](https://blog.nearlyfreespeech.net/2021/01/19/free-speech-in-2021/).

I use [a modified version](https://git.eyoo.link/ethan/geronimo) of [smol](https://github.com/colorchestra/smol/) as the website's theme.

# For a local version of the website

1. `git clone https://git.eyoo.link/ethan/ethanyoo.com someFolder`
2. `cd someFolder`
3. `hugo server --openBrowser` or [its alias](https://discourse.gohugo.io/t/hugo-serve-vs-hugo-server/24872) `hugo serve --openBrowser`

# To browse the source code

This project is pushed or mirrored to three remote repositories.

* [Codeberg](https://codeberg.org/) is "a non-profit, community-led effort that provides [**Git hosting**](https://codeberg.org/ethan) and [other services](https://docs.codeberg.org/getting-started/what-is-codeberg/) for free and open source projects."
* Codeberg also backs the [Forgejo software](https://forgejo.org/), which I [**self-host for personal use**](https://git.eyoo.link/ethan).
* [Framasoft](https://framasoft.org/) is a French nonprofit organization actively engaged in free culture and social justice movements. Framasoft hosts [**a GitLab instance**](https://framagit.org/ethan).

# To submit feedback, suggest specific changes, etc.

* Email (preferred)
* Codeberg

# Licensing

SPDX-License-Identifier: `CC-BY-SA-4.0`

**Original textual content** is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License,](https://creativecommons.org/licenses/by-sa/4.0/) the full text of which is available in the `LICENSE` file.

I post **limited excerpts of copyrighted content** under [the fair use doctrine.](https://www.copyright.gov/fair-use/more-info.html)

**Images** are licensed on a per-file basis, possibly using [small print.](https://html.spec.whatwg.org/multipage/text-level-semantics.html#the-small-element) Icons (e.g., on the ["Resources" page](https://ethanyoo.com/resources/) were downloaded from [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page), the [walkxcode/dashboard-icons](https://github.com/walkxcode/dashboard-icons/blob/main/LICENSE) repository, and the corresponding source code or website.

## Theme: [MIT (Expat) License](https://choosealicense.com/licenses/mit/)

Copyright &copy; 2016 Vimux

Copyright &copy; 2020 colorchestra

Copyright &copy; 2021 Ethan Yoo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
